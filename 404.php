<?php get_header(); ?>
<main role="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <article id="post-404">
                    <h1><?php _e( 'Stránka nenalezena', 'eso-theme' ); ?></h1>
                    <h2>
                        <a href="<?php echo home_url(); ?>"><?php _e( 'Zpět na hlavní stránku', 'eso-theme' ); ?></a>
                    </h2>
                </article>
            </div>
        </div>
    </div>
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
