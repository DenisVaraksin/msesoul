<?php get_header(); ?>

<main role="main">
    <!-- Products Header Picture -->
    <section id="seznam_produktu" class="padding-section">
        <div class="top_image" style="background: url(<?php echo get_template_directory_uri() . "/img/seznam_produktu.jpg"?>)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Produkty</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>


	<?php get_template_part('loop-esoul_product'); ?>



</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
