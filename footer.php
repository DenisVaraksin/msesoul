<footer class="footer pt-5 pb-5 justify-content-center" role="contentinfo">
    <div class="container">

        <div class="row">
            <div class="col-md-8 footer-line">
                <p class="copyright">
                    &copy; <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>
                </p>

                <div class="footer_menu">

                    <?php wp_nav_menu( array(
                        'theme_location'  => 'footer-menu',
                        'menu_class'      => 'navbar-nav ',
                        'depth'           => 2,
                        'container_class' => 'navbar-collapse',
                        'container_id'    => 'bs-example-navbar-collapse-1',
                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'          => new WP_Bootstrap_Navwalker()
                    ) ); ?>
                </div>

            </div>
            <div class="col-md-4 footer-line">
                <a class="float-right logo_footer" href="#"><img src="<?php echo get_template_directory_uri() . "/img/logo_white.png"?>"></a>
            </div>
        </div>
    </div>
</footer>

<?php if ( current_user_can( 'install_plugins' ) ) : ?>
    <div id="show-frontend-dashboard" class="btn btn-primary"><i class="fas fa-tachometer-alt"></i> <?php _e("Přehled", "eso") ?></div>
    <div id="eso-frontend-dashboard"></div>
<?php endif; ?>

</div>

<?php wp_footer(); ?>
<?php do_action( 'eso_body_end_hook' ); ?>
</body>
</html>
