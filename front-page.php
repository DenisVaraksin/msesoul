<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<section id="homepage" class="padding-section">
	<?php
    // content is being rendered in Gutenberg Main Page
    the_content();
	/*
	$args   = array(
		'post_type'   => 'slider',
		'post_status' => 'publish',
        'posts_per_page' => -1
	);
	$slides = new WP_query ( $args );
	if ( $slides->have_posts() ) {
		*/
		?>
		<?php
		/*
        <ul class="homeslider">
				<?php
				while ( $slides->have_posts() ) {
					$slides->the_post();
					?>
                    <li>
                        <img src="<?php //echo get_the_post_thumbnail_url(null, "photo"); ?>" />
                    </li>
					<?php
				}
				wp_reset_postdata();
				?>
        </ul>
        */?>
	<?php 
	//}
	
	?>

    <!-- first "SLIDER" section to be commented to the block -->
	<!--
    <section id="slider">
		<?php
		//for ($i=0; $i < 5; $i++) {
		?>
		<div class="single_slide" style="background: url(<?php// echo get_template_directory_uri() . "/img/slider_header.jpg"?>)">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="over_slide">
							<h1>Buď <strong>originální</strong></h1>
							<p>Rekni ne šabloně a vytvoř si e-shop na míru</p>
							<a href="#" class="black_button">Mám zájem</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		//}
		?>
		
	</section>
    -->

    <!-- second "ADVANTAGES" section to be commented to the block -->
	<!-- <section id="list_ico">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<img class="ico_single" src="<?php// echo get_template_directory_uri() . "/img/delivery-truck.svg"?>">
					<p class="title_par">Dodání do 24 hod.</p>
					<p>Nejvíce popularizováno byl Lorem Ipsum v šedesátých letech 20. století, kdy byly.</p>	
				</div>
				<div class="col-md-3">
					<img class="ico_single" src="<?php// echo get_template_directory_uri() . "/img/package.svg"?>">
					<p class="title_par">1000+ produktů skladem</p>
					<p>Je obecně známou věcí, že člověk bývá při zkoumání grafického.</p>

				</div>
				<div class="col-md-3">
					<img class="ico_single" src="<?php// echo get_template_directory_uri() . "/img/telephone.svg"?>">
					<p class="title_par">Zákaznický servis</p>
					<p>Existuje mnoho variant s pasážemi Lorem Ipsum, nicméně valná většina.</p>
				</div>
				<div class="col-md-3">
					<img class="ico_single" src="<?php// echo get_template_directory_uri() . "/img/tag.svg"?>">
					<p class="title_par">Sezónní slevy</p>
					<p>Všechny generátory Lorem Ipsum na internetu mají tendenci opakovat kusy.</p>
				</div>
			</div>
		</div>
	</section>
    -->

    <!-- third section "NEWEST PRODUCTS" section to be commented to the block -->
	<!--<section id="new_product" class="product_list_home">
		<div class="container">
			<h2>Nejnovější produkty</h2>
			<div class="row">
				<?php
				//for ($i=0; $i < 4; $i++) {
				?>
				<a href="<?php// echo home_url("/produkty/lampa-copper/")?>" class="col-md-3 single_product">
					<div class="over_product">
						<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Nové</div></div>
						<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Doprava zdarma</div></div>

						<div class="product_image">
						<img width="250" height="159" src="<?php// echo get_template_directory_uri() . "/img/lampa.jpg"?>" class="attachment-small size-small" alt="" srcset="<?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 250w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 90w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 50w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 476w" sizes="(max-width: 250px) 100vw, 250px">

						</div>

						<h3 class="product-item__title">Lampa Copper</h3>
						<div class="status">
							<span class="font-weight-bold stock-status--in-stock">Skladem</span>
						</div>

						<div class="price price--list">
							<span class="price__item">2550 Kč</span>
				        </div>

						<div class="row action_detail">
							<div class="col-md-6 cart_in">
								<img src="<?php// echo get_template_directory_uri() . "/img/shopping-cart.svg"?>">
								<p>Do košíku</p>
							</div>
							<div class="col-md-6">
								<div class="black_button_out">Detail</div>
							</div>
						</div>

					</div>
				</a>
				<?php
				//}
				?>
			</div>
			<div class="center_button">
				<a href="<?php// echo home_url("/seznam-produktu/")?>" class="black_button all_product">Všechny produkty</a>
			</div>
		</div>	
	</section>
	-->

    <!-- fourth "BANNER" section to be commented to the block -->
	<?php
	// require get_template_directory() . '/include/banner.php';
	?>

    <!-- fifth "MOST POPULAR PRODUCTS" section to be commented to the block -->
    <!--
	<section id="most_product" class="product_list_home">
		<div class="container">
			<h2>Nejprodávanější produkty</h2>
			<div class="row">
				<?php
				//for ($i=0; $i < 4; $i++) {
				?>
				<a href="<?php// echo home_url("/produkty/lampa-copper/")?>" class="col-md-3 single_product">
					<div class="over_product">
						<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Nové</div></div>
						<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Doprava zdarma</div></div>

						<div class="product_image">
						<img width="250" height="159" src="<?php// echo get_template_directory_uri() . "/img/lampa.jpg"?>" class="attachment-small size-small" alt="" srcset="<?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 250w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 90w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 50w, <?php// echo get_template_directory_uri() . "/img/lampa.jpg"?> 476w" sizes="(max-width: 250px) 100vw, 250px">

						</div>

						<h3 class="product-item__title">Lampa Copper</h3>
						<div class="status">
							<span class="font-weight-bold stock-status--in-stock">Skladem</span>
						</div>

						<div class="price price--list">
							<span class="price__item">2550 Kč</span>
				        </div>

						<div class="row action_detail">
							<div class="col-md-6 cart_in">
								<img src="<?php// echo get_template_directory_uri() . "/img/shopping-cart.svg"?>">
								<p>Do košíku</p>
							</div>
							<div class="col-md-6">
								<div class="black_button_out">Detail</div>
							</div>
						</div>

					</div>
				</a>
				<?php
				//}
				?>
			</div>
			<div class="center_button">
				<a href="<?php// echo home_url("/seznam-produktu/")?>" class="black_button all_product">Všechny produkty</a>
			</div>
		</div>
	</section>
    -->
    
</section>

    <?php  endwhile; endif; ?>
<?php get_footer(); ?>
