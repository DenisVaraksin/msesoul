<?php
$theme = wp_get_theme();
define('THEME_VERSION', $theme->Version);

require( "templates/emails/functions.php" );

if ( function_exists( 'add_theme_support' ) ) {
	// Add Menu Support
	add_theme_support( 'menus' );

	// Add Thumbnail Theme Support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'photo', 1440, '', true ); // Container width Thumbnail
	add_image_size( 'large', 750, '', true ); // Large Thumbnail
	add_image_size( 'medium', 500, '', true ); // Medium Thumbnail
	add_image_size( 'small', 250, '', true ); // Small Thumbnail
	add_image_size( 'little', 110, 110, true ); // Small Thumbnail
	add_image_size( 'tiny', 90, '', true ); // Small Thumbnail
	add_image_size( 'table', 50, '', true ); // Small Thumbnail

	// Enables post and comment RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Localisation Support
	load_theme_textdomain( 'theme', get_template_directory() . '/languages' );
}
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
// Register navigation
function eso_register_menu_location() {
	register_nav_menus( array( // Using array to specify more menus if needed
		'header-menu' => __( 'Header Menu', 'theme' ), // Main Navigation
		'top-menu'    => __( 'Top Menu', 'theme' ), // Main Navigation
		'footer-menu'    => __( 'Footer Menu', 'theme' ) // Main Navigation
	) );
}

add_action( 'init', 'eso_register_menu_location' );

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter( $var ) {
	return is_array( $var ) ? array() : '';
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class( $classes ) {
	global $post;
	if ( is_home() ) {
		$key = array_search( 'blog', $classes );
		if ( $key > - 1 ) {
			unset( $classes[ $key ] );
		}
	} elseif ( is_page() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	} elseif ( is_singular() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	}

	return $classes;
}

// If Dynamic Sidebar Exists
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebar( array(
		'name'          => __( 'Zápatí 1', 'eso' ),
		'id'            => 'footer-area-1',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );

	register_sidebar( array(
		'name'          => __( 'Zápatí 2', 'eso' ),
		'id'            => 'footer-area-2',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );

	register_sidebar( array(
		'name'          => __( 'Zápatí 3', 'eso' ),
		'id'            => 'footer-area-3',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );

	register_sidebar( array(
		'name'          => __( 'Small_Slider', 'eso' ),
		'id'            => 'small-slider',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );
}

function remove_admin_bar() {
	return false;
}

add_filter( 'show_admin_bar', 'remove_admin_bar' );

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet


// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination() {
	global $wp_query;
	$big = 999999999;
	echo paginate_links( array(
		'base'    => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
		'format'  => '?paged=%#%',
		'current' => max( 1, get_query_var( 'paged' ) ),
		'total'   => $wp_query->max_num_pages
	) );
}

function theme_styles() {
	if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {
		wp_enqueue_script( 'bsjs', "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", array( 'jquery' ), "1", true );
		wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), "1", true );

		if(is_single() && get_post_type() == "esoul_product"){
			//Lightbox
			wp_enqueue_style( 'lightbox', '//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css');
			wp_enqueue_script( 'lightbox', '//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js', array( 'jquery' ), "1", true );
			//Lightbox END

			wp_enqueue_script( 'product-single', get_template_directory_uri() . '/js/product/single.min.js', array( 'jquery' ), THEME_VERSION, true );
		}

		wp_enqueue_style( 'theme', get_template_directory_uri() . '/style.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'bs', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'eso-main', get_template_directory_uri() . '/css/main.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'eso-responsive', get_template_directory_uri() . '/css/responsive.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'eso-product', get_template_directory_uri() . '/css/product.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'eso-eshop', get_template_directory_uri() . '/css/eshop.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_style( 'tom-eshop', get_template_directory_uri() . '/css/tom_shop.css', array(), THEME_VERSION, 'all' );

		wp_enqueue_script( 'slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), THEME_VERSION, true );

		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.min.js', array( 'jquery' ), THEME_VERSION, true );

		if ( function_exists( "eso_get_page_id" ) && get_the_ID() == eso_get_page_id( "cart" ) ) {
			wp_enqueue_style( 'cart-css', get_template_directory_uri() . '/css/cart.css', array(), THEME_VERSION, 'all' );

			wp_enqueue_script( 'cart-js', get_template_directory_uri() . '/js/cart.min.js', array( 'jquery' ), THEME_VERSION, false );
		}

		if ( function_exists( 'eso_is_module_active' ) ) {
			if ( eso_is_module_active( "instagram_feed" ) ) {
				wp_enqueue_style( 'eso-instagram-feed', get_template_directory_uri() . '/css/modules/instagram-feed.css', array(), THEME_VERSION, 'all' );
			}

			if ( eso_is_module_active( "facebook_feed" ) ) {
				wp_enqueue_style( 'eso-facebook-feed', get_template_directory_uri() . '/css/modules/facebook-feed.css', array(), THEME_VERSION, 'all' );
			}
		}
	}
}

add_action( 'wp_enqueue_scripts', 'theme_styles' ); // Add Theme Stylesheet

// Remove Actions
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // Index link
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'rel_canonical' );

// Add Filters
add_filter( 'body_class', 'add_slug_to_body_class' ); // Add slug to body class (Starkers build)
add_filter( 'widget_text', 'do_shortcode' ); // Allow shortcodes in Dynamic Sidebar
add_filter( 'widget_text', 'shortcode_unautop' ); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter( 'the_excerpt', 'shortcode_unautop' ); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter( 'the_excerpt', 'do_shortcode' ); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter( 'style_loader_tag', 'html5_style_remove' ); // Remove 'text/css' from enqueued stylesheet
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to thumbnails
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter( 'the_excerpt', 'wpautop' ); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode( 'html5_shortcode_demo', 'html5_shortcode_demo' ); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode( 'html5_shortcode_demo_2', 'html5_shortcode_demo_2' ); // Place [html5_shortcode_demo_2] in Pages, Posts now.

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/marketsoul/hello-theme',
	__FILE__,
	'hello-theme'
);

$myUpdateChecker->setAuthentication('xKxg4MzJzyHmb4xCBA9v');

//Bootstrap 4 nav menu
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

function eso_after_setup_theme() {
	$site_type = get_option( 'show_on_front' );
	if ( $site_type == 'posts' ) {
		$front_page = [
			'post_title'   => __( "Hlavní stránka", "eso" ),
			'post_content' => "",
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_type'    => 'page',
			'post_name'    => 'home'
		];

		$archive = [
			'post_title'   => __( "Produkty", "eso" ),
			'post_content' => "",
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_type'    => 'page',
			'post_name'    => 'produkty'
		];

		$front_page_id = wp_insert_post( $front_page );
		$archive_id    = wp_insert_post( $archive );

		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $front_page_id );
		update_option( 'page_for_posts', $archive_id );
	}
}

add_action( 'after_setup_theme', 'eso_after_setup_theme' );

function custom_gallery( $string, $attr ) {
	$output = "<div id='\"container\"'>";
	$posts  = get_posts( array( 'include' => $attr['ids'], 'post_type' => 'attachment' ) );

	foreach ( $posts as $imagePost ) {
		$output .= "<div src='" . wp_get_attachment_image_src( $imagePost->ID, 'small' )[0] . "'>";
		$output .= "<div src='" . wp_get_attachment_image_src( $imagePost->ID, 'medium' )[0] . "' data-media=\"(min-width: 400px)\">";
		$output .= "<div src='" . wp_get_attachment_image_src( $imagePost->ID, 'large' )[0] . "' data-media=\"(min-width: 950px)\">";
		$output .= "<div src='" . wp_get_attachment_image_src( $imagePost->ID, 'extralarge' )[0] . "' data-media=\"(min-width: 1200px)\">";
	}

	$output .= "</div>";

	return $output;

}

add_filter( 'post_gallery', 'customFormatGallery', 10, 2 );

//registration of new blocks for ACF
if (function_exists('acf_register_block_type')) {
	add_action('acf/init', 'register_acf_block_types');
}

function register_acf_block_types() {
	/* Homepage */
	acf_register_block_type (
		array(
			'name' => 'front-slider',
			'title' => __('Front SLider'),
			'description' => __('A Custom Slider Block'),
			'render_template' => 'templates/blocks/homepage/slider.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('slider', 'product'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-advantages',
			'title' => __('Msesoul Advantages'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/homepage/advantages.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('advantages', 'single', 'product'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-newest-products',
			'title' => __('Msesoul First Products'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/homepage/firstproducts.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('newest', 'single', 'product'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-contact',
			'title' => __('Msesoul Contact'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/homepage/contact.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('contact', 'single'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-popular-products',
			'title' => __('Msesoul Products Row'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/homepage/productsrow.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('popular', 'single', 'product'),
		)
	);
	/* O Nas */

	acf_register_block_type (
		array(
			'name' => 'msesoul-about',
			'title' => __('Msesoul About'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/onas/onas.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('about', 'msesoul', 'product'),
		)
	);
	/* Kontakt */

	acf_register_block_type (
		array(
			'name' => 'msesoul-contact-information',
			'title' => __('Msesoul Kontakt Information'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/kontakt/information.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('contact', 'msesoul', 'information'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-contact-form',
			'title' => __('Msesoul Kontakt Form'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/kontakt/contact-form.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('contact', 'msesoul', 'form'),
		)
	);

	acf_register_block_type (
		array(
			'name' => 'msesoul-contact-map',
			'title' => __('Msesoul Kontakt Map'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/kontakt/contact-map.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('contact', 'msesoul', 'map'),
		)
	);

	/* Products */
	acf_register_block_type (
		array(
			'name' => 'msesoul-single-product',
			'title' => __('Msesoul Single Product'),
			'description' => __('A Custom Content Block'),
			'render_template' => 'templates/blocks/product/single.php',
			'icon' => 'editor-paste-text',
			'keywords' => array('single', 'msesoul', 'product'),
		)
	);
}





//new posttype for slider added
/*
function create_posttype() {
	register_post_type( 'slider',
		array(
			'labels'      => array(
				'name'          => __( 'Slider' ),
				'singular_name' => __( 'Slides' )
			),
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => array( 'slug' => 'homeslider' ),
			'supports'    => array( 'title', 'thumbnail' ),
		)
	);
}
*/
add_action( 'init', 'create_posttype' );


?>
