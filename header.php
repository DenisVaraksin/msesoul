<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
			echo ' – ';
		} ?><?php bloginfo( 'name' ); ?></title>
    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<?php if ( get_option( "eso_store_favicon" ) ) : $favicon_id = get_option( "eso_store_favicon" ); ?>
        <link rel="apple-touch-icon" sizes="180x180"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 180, 180 ] );
		      echo $favicon[0] ?>">
        <link rel="icon" type="image/png" sizes="192x192"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 192, 192 ] );
		      echo $favicon[0] ?>">
        <link rel="icon" type="image/png" sizes="96x96"
              href="<?php $favicon = wp_get_attachment_image_src( $favicon_id, [ 90, 90 ] );
		      echo $favicon[0] ?>">
	<?php endif; ?>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'eso_body_start_hook' ); ?>
<?php wp_nonce_field( ESO_NONCE ); ?>
<div class="wrapper">
    <!-- adjust header here -->
    <header class="header clear" role="banner">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="<?php echo home_url( "/" ); ?>">
							<?php if ( get_option( "eso_store_logo" ) ) : ?>
								<?php echo wp_get_attachment_image( get_option( "eso_store_logo" ), 'full' ) ?>
							<?php else: ?>
								<?php echo get_bloginfo( 'name' ); ?>
							<?php endif; ?>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse " id="navbarSupportedContent">
							<?php wp_nav_menu( array(
								'theme_location'  => 'header-menu',
								'menu_class'      => 'navbar-nav menu-left',
								'depth'           => 2,
								'container_class' => 'navbar-collapse',
								'container_id'    => 'bs-example-navbar-collapse-1',
								'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
								'walker'          => new WP_Bootstrap_Navwalker()
							) ); ?>
                            <ul class="navbar-nav ml-auto mr-lg-3">
                                <li class="nav-item menu-item menu-cart">
                                    <a href="<?php echo home_url( '/cart' ) ?>" class="nav-link custom-cart" >

                                        <img class="nav-picture" title='<?php __( 'Košík', 'eso' ) ?>' src="<?php echo get_stylesheet_directory_uri() ?>/img/icons/shopping-cart.svg"/>

                                        <span class='d-lg-none nav-link'><?php __( 'Košík', 'eso' ) ?></span> <span
                                                id="cart-sum"><?php $cart = new Eso_Cart( eso_session_token() );
											echo round($cart->get_sum( true, true )) . ' ';
                                            echo eso_get_active_currency_symbol();
											?></span>
                                    </a>

                                </li>
                                <li class="nav-item menu-item menu-account dropdown menu-item-has-children">
                                    <a href="#" data-toggle="dropdown" aria-haspopup='true' aria-expanded='false'
                                       class='dropdown-toggle nav-link'>

                                        <!-- <i class='far fa-user' title='<?php _e( 'Můj účet', 'eso' ) ?>'></i> -->
                                        <img class="nav-picture-account" title='<?php _e( 'Můj účet', 'eso' ) ?>' src="<?php echo get_stylesheet_directory_uri() ?>/img/icons/user.svg"/>

                                        <span class='d-lg-none'><?php __( 'Můj účet', 'eso' ) ?></span></a>
                                    <ul class="dropdown-menu my-account-menu-list">
										<?php if ( is_user_logged_in() ) : ?>
                                            <li class='menu-item nav-item'>
                                                <a href="<?php eso_the_page_link( 'account' ); ?>"
                                                   class="dropdown-item"><?php _e( "Můj účet", "eso" ) ?></a>
                                            </li>
                                            <li class="menu-item nav-item">
                                                <a href="<?php eso_the_page_link( 'orders' ); ?>"
                                                   class="dropdown-item"><?php _e( "Objednávky", "eso" ) ?></a>
                                            </li>
                                            <li class="menu-item nav-item">
                                                <a href="<?php echo wp_logout_url(); ?>"
                                                   class="dropdown-item"><?php _e( "Odhlásit se", "eso" ) ?></a>
                                            </li>
										<?php else : ?>
                                            <li class='menu-item nav-item'>
                                                <a href="<?php eso_the_page_link( 'login' ); ?>"
                                                   class="dropdown-item"><?php _e( "Přihlásit se", "eso" ) ?></a>
                                            </li>
										<?php endif ?>
                                    </ul>
                                </li>
                                <li class="nav-item menu-item">
	                                <?php
	                                $customer = new Eso_Customer( get_current_user_id() );
                                    if ( is_user_logged_in() ) : ?>
                                        <a href="<?php eso_the_page_link( 'account' ); ?>" class="nav-link">
                                            <?php echo $customer->get_shipping_first_name(); ?>
                                        </a>
	                                <?php endif ?>
                                </li>
                            </ul>
							<?php echo do_shortcode( '[eso_currency_switcher]' ); ?>
<!--                            <form class="form-inline my-2 my-lg-0">-->
<!--								--><?php //get_template_part( 'searchform' ); ?>
<!--                            </form>-->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>