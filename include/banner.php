<section id="info_banner" style="background: url(<?php echo get_template_directory_uri() . "/img/info_banner2.jpg"?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Potřebujete poradit? Neváhejte nás kontaktovat.</h2>
				<div class="center_button">
					<a href="<?php echo home_url("/kontakt/")?>" class="black_button all_product">Kontakt</a>
				</div>
			</div>
		</div>
	</div>
</section>