;(function ($) {
    $(document).ready(function () {

        $('.payment-method input[type="radio"]:checked').each(function () {
            let payment_method = $(this).closest("label").find('.payment-method__name').text();
            $("#payment_method").text(payment_method);
        });

        $('.payment-method input[type="radio"]').change(function () {
            let payment_method = $(this).closest("label").find('.payment-method__name').text();
            $("#payment_method").text(payment_method);
        });

        $('.shipping-method input[type="radio"]:checked').each(function () {
            let shipping_method = $(this).closest("label").find('.shipping-method__name').text();
            $("#shipping_method").text(shipping_method);
        });

        $('.shipping-method input[type="radio"]').change(function () {
            let shipping_method = $(this).closest("label").find('.shipping-method__name').text();
            $("#shipping_method").text(shipping_method);
        });
    });
})(jQuery);