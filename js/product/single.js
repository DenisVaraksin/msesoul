;(function ($) {
    $(document).ready(function () {
        lightbox.option({
            'resizeDuration': 0,
            'wrapAround': true,
            'showImageNumberLabel': false,
            'imageFadeDuration': 0
        });
    });
})(jQuery);