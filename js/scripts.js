(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        $(".eso-product-quantity").change(function () {
            let new_value = $(this).val();
            $(this).attr("value", new_value);
        });

        $('.homeslider').slick({
            infinite: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: false,
            nextArrow: false
        });

        // hide last paragraph at contact page
        //$('#kontakt').next().addClass('d-none');

        //display DETAILS on product picture hover
        $('.product_image').hover(function() {
            $( this ).find('.produc_image-hover').removeClass( "d-none" );
        }, function() {
            $( this ).find('.produc_image-hover').addClass( "d-none" );
        });

        $('.product-gallery__main').hover(function() {
            $( this ).find('.produc_image-hover').removeClass( "d-none" );
        }, function() {
            $( this ).find('.produc_image-hover').addClass( "d-none" );
        });

        //Obchodni Podminky -- open/close Sub Categories
        $('.categories-main .main_cat .category-link').click(function(e) {
            e.preventDefault();
            /* toggle on Child Categories*/
            $(this).parent().parent().next().toggleClass('d-none');
            /* toggle on Arrows */
            let blcokStatus = $(this).parent().attr("class");
            console.log( blcokStatus );

            $(this).children().closest('.arrow-down').toggleClass('d-none');
            $(this).children().closest('.arrow-up').toggleClass('d-none');
        });

        //JavaScript to hide words over certain amount
        // Show more text option -campaing page
        $(document).ready(function(){
            var maxLength = 300;
            $(".show-read-more").each(function(){
                var myStr = $(this).text();
                if($.trim(myStr).length > maxLength){
                    var newStr = myStr.substring(0, maxLength);
                    var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                    $(this).empty().html(newStr);
                    $(this).append(' <a href="javascript:void(0);" class="read-more">... číst více</a>');
                    $(this).append('<span class="more-text">' + removedStr + '</span>');
                }
            });
            $(".read-more").click(function(){
                $(this).siblings(".more-text").contents().unwrap();
                $(this).remove();
            });
        });

        // Single Product related Products Layout Change ++ layout change accorfing to amount of products
        $('#related-products-call .single_product').removeClass('col-md-4').addClass('col-lg-3').addClass('col-md-4');

        let countTest = $('#related-products-call .single_product').length;

        if ( countTest == 3) {
            $('#related-products-call .first-row').prepend( "<div class='col-lg-1'></div>" );
        }

        if ( countTest == 2) {
            $('#related-products-call .first-row').prepend( "<div class='col-lg-3'></div>" );
        }

        if ( countTest == 1) {
            $('#related-products-call .first-row').prepend( "<div class='col-lg-4'></div>" );
        }

    });
})(jQuery, this);