;(function($){
$(document).ready(function() {

$('#slider').slick({
    draggable: true,
    dots: true,
    pauseOnHover: true,  
    pauseOnFocus: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 3000,
});

$('#slider_box').slick({
    draggable: true,
    dots: false,
    pauseOnHover: true,  
    pauseOnFocus: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
});

    $('#slider_box_small').slick({
        draggable: true,
        dots: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
    });

});
})(jQuery);