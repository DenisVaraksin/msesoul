<!-- Actually "Products Page" -->
<section id="list_product_page">
    <div class="container">
        <div class="row">
            <div class="col col-lg-3">
                <!-- Categories -->
                <section id="category_list">
                    <div class="single_cat">
                        <div class="main_cat <?php
                        // "VSE" becomes Active Link if Path euqlas to "/msesoul/produkty/" and category_id is NOT SET
                        if (!isset($_GET["category_id"])){
                            echo (parse_url($_SERVER['REQUEST_URI'])['path'] === '/produkty/' ? 'active-link-wrapper' : null);
                        }
                        ?>">
                            <a href="<?php echo get_post_type_archive_link("esoul_product"); ?>">Vše</a>
                        </div>
                    </div>


                    <div class="test-category-wrapper">
                        <?php
                        $terms=eso_get_product_categories( '0', [], $hide_empty = false );
                        foreach (  $terms as $term  ) :
                            if( isset($_GET["category_id"]) ) {
                                $catId = (int) $_GET["category_id"];
                            }
                            ?>
                            <?php
                            //showing only categories that do not have parents
                            if($term->parent != true) {
                            $children = eso_get_product_categories($term->term_id );
                             ?>
                            <div id="termid-<?php echo $term->term_id; ?> " class="categories-main">
                                <div class="main_cat <?php
                                //class for an active link if child cat is active
                                foreach($children as $child) {
                                    if($catId == $child->term_id) {
                                        echo 'active-link-wrapper';
                                    }
                                }
                                //class for an active link if category is selected
                                if($catId == $term->term_id) {
                                    echo 'active-link-wrapper';
                                }
                                ?>">
                                    <a href="?category_id=<?php echo $term->term_id ?>" class='<?php if($catId == $term->term_id) { ?>active-link <?php } ?>
                                    <?php if(sizeof($children) > 0) { // functionality of a category tree if it has child in scripts.js ?>
                                     category-link
                                     <?php } ?>'>
                                        <?php echo $term->name ?>
                                    </a>
                                    <?php
                                    //if Category has children show arrows
                                    if ( sizeof($children) > 0 ) { ?>
                                    <p class="arrow-down">
                                        &#8964;  <!-- arrow down -->
                                    </p>
                                    <p class="d-none arrow-up">   <!-- was class d-none before -->
                                        ⌃
                                    </p>
                                    <?php } ?>
                                </div>
                            </div>
                                <?php
                                }
                                ?>
                            <div class="child-categories <?php
                            // adding 'd-none' Class to "Child Categories" block if "Child Categories" are not active
                            $classArray = array();
                            foreach($children as $child) {
                                if ($catId == $child->term_id) {
                                    // if Child Category is active make classArray empty and break!
                                    $classArray = array();
                                    break;
                                } else {
                                    // if Child Category is not active add 'd-none' class
                                    if( !in_array('d-none',$classArray)) array_push($classArray, 'd-none');
                                }
                            }
                            // printing Array values for the class
                            foreach ($classArray as $key => $value)
                            {
                                echo $value;
                            }
                            ?>">
                                    <?php
                                    //loop for child categories to display them
                                    foreach($children as $child) {
                                        ?>
                                        <div id="termid-<?php echo $child->term_id; ?>">
                                            <div class="sub_cat">
                                                <a href="?category_id=<?php echo $child->term_id ?>" class='<?php if($catId == $child->term_id) { ?>active-link <?php } ?> '>
                                                    <?php echo $child->name ?>
                                                </a> &nbsp;
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                             </div>
                            <?php endforeach; ?>
                    </div>
                </section>

                <!-- Sidebars -->
                <section id="sidebar_banner" style="background: url(<?php echo get_template_directory_uri() . "/img/sidebar_banner.jpg"?>)">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="banner_title">Potřebujete poradit? <br/>Kontaktujte nás.</p>

                                <div class="contact_box">
                                    <a href="tel: +420 123 456 789"><i class="fas box_ico fa-phone-square"></i>+420 123 456 789</a>
                                    <br/>
                                    <a href="mailto: info@e-soul.cz"><i class="fas box_ico fa-envelope-square"></i><span>info@e-soul.cz</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="slider_box">
                    <?php
                    //for ($i=0; $i < 5; $i++) {
                        ?>
                        <div class="single_box_slide">
                            <div class="over_box_slide">
                                <img class="ico_single" src="<?php echo get_template_directory_uri() . "/img/delivery-truck.svg"?>">
                                <p class="box_title">Dodání do 24 hod.</p>
                                <p>Nejvíce popularizováno byl Lorem Ipsum v šedesátých letech 20. století, kdy byly.</p>
                            </div>
                        </div>
                        <div class="single_box_slide">
                            <div class="over_box_slide">
                                <img class="ico_single" src="<?php echo get_template_directory_uri() . "/img/delivery-truck.svg"?>">
                                <p class="box_title">Dodání do 24 hod.</p>
                                <p>Nejvíce popularizováno byl Lorem Ipsum v šedesátých letech 20. století, kdy byly.</p>
                            </div>
                        </div>

                        <?php
                 //  }
                    ?>
                </section>

                <!--  Custom Section display ( didnt work with ACF )


                </section>
                -->

            </div>

            <!-- Actually Calling Products -->
            <div class="col col-lg-9">


                <section class="product_list_home">
                    <div class="container">
                        <div class="row">
                            <?php
                            $args = [
                                "post_type"   => "esoul_product",
                                "post_status" => "publish",
                                "posts_per_page" => -1
                            ];

                            ( ! empty( $_GET["category_id"] ) ) ? $args["tax_query"] = [
                                [
                                    "taxonomy" => "product_category",
                                    "terms"    => (int) $_GET["category_id"]
                                ]
                            ] : null;

                            $the_query = new WP_Query( $args );

                            if ( $the_query->have_posts() ):
                            while ( $the_query->have_posts() ) : $the_query->the_post();


                            $product = new Eso_Product( get_the_ID() ); ?>
                                <!-- calling Products Template from list-item.php-->
                                <?php echo $product->get_list_item_template(); ?>
                            <?php

                            endwhile;
                            else: ?>

                                <h2><?php _e( 'Zatím žádné produkty.', 'eso-theme' ); ?></h2>
                            <?php endif; ?>
                        </div>

                        <!-- Pagination -->
                        <div class="container" id="loop_product_pagination">
                            <div class="row">
                                <div class="col">
                                    <?php get_template_part('pagination'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="center_button">
                            <a href="<?php echo home_url("/produkty/")?>" class="black_button all_product">Všechny produkty</a>
                        </div>
                    </div>
                </section>


            </div>
        </div>
    </div>
</section>