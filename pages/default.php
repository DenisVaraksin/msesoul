<?php get_template_part("pages/header"); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <section class="pt-2">
				<?php the_content(); ?>
            </section>
        </div>
    </div>
</div>

