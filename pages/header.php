<div class="thumbnail-header <?php if ( has_post_thumbnail( get_the_ID() ) )
	echo "has-background-image" ?>"
     style="background-image: url(<?php echo get_the_post_thumbnail_url( get_the_ID(), "photo" ) ?>)">
    <div class="container">
        <div class="row">
            <div class="col p3 m-5 text-center">
                <h1 class="has-large-font-size font-weight-bold">
					<?php the_title(); ?>
                </h1>
            </div>
        </div>
    </div>
</div>