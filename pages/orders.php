<?php
if ( ! is_user_logged_in() ) {
	wp_redirect( home_url() );
	die();
}
$customer = new Eso_Customer( get_current_user_id() );
get_template_part( "pages/header" );
?>
<div class="container">
    <div class="row p-2">
        <div class="col text-center">
			<?php if ( $customer->has_orders() ) : ?>
                <table class="table table-responsive">
                    <tbody>
                    <tr>
                        <th scope="col"><?php _e( "Objednávka", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Datum", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Cena", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Stav", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Akce", "eso" ) ?></th>
                    </tr>
					<?php
					/* @var $order Eso_Order */
					foreach ( $customer->get_orders() as $order ) : ?>
                        <tr class="order-row">
                            <td class="order-row__title">
								<?php echo $order->get_title(); ?>
                            </td>
                            <td class="order-row__date">
								<?php echo $order->get_date_and_time_created(); ?>
                            </td>
                            <td class="order-row__price">
								<?php echo $order->get_total(true); ?>
                            </td>
                            <td>
                                <span class="order-status--<?php echo $order->get_status_slug() ?>"><?php echo $order->get_status_name() ?></span>
                            </td>
                            <td class="order-row__detail">
                                <a href="<?php echo get_permalink( $order->get_id() ); ?>"
                                   class="btn btn-lg"><?php _e( "Detail objednávky", "eso" ) ?></a>
                            </td>
                        </tr>
					<?php endforeach;
					wp_reset_postdata();
					?>
                    </tbody>
                </table>

			<?php else: ?>
                <p><?php _e( "Zatím nemáte žádné objednávky", "eso" ) ?></p>
                <a href="<?php echo get_post_type_archive_link( "esoul_product" ) ?>"
                   class="btn btn-lg btn-dark"><?php _e( "Procházet zboží", "eso" ) ?></a>
			<?php endif; ?>
        </div>
    </div>
</div>