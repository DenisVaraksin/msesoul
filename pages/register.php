<div class="container">
	<div class="row">
		<div class="col">
			<ul class="nav nav-pills justify-content-center mb-5" id="login-nav">
				<li class="nav-item">
					<a class="nav-link" href="<?php eso_the_page_link('login') ?>"><?php _e("Přihlásit se", "eso") ?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="#"><?php _e("Nový účet", "eso") ?></a>
				</li>
			</ul>
			<div id="login-register" class="enter-form">
				<?php echo do_shortcode("[eso_register_form]") ?>
			</div>
		</div>
	</div>
</div>