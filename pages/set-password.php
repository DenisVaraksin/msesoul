<div class="container">
	<div class="row">
		<div class="col-md-4 offset-md-4 mt-5">
			<h1><?php the_title() ?></h1>
			<?php echo do_shortcode("[eso_set_password]") ?>
		</div>
	</div>
</div>