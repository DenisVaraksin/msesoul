<?php
/**
 * @var $order Eso_Order
 */
?>
<div class="container">
    <div class="row">
        <div class="col text-center">
            <h1 class="has-large-font-size font-weight-bold"><?php _e( "Vaše objednávka byla stornována", "eso" ) ?></h1>
            <div id="order_detail" class="row">
                <div class="col-md-12">
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Číslo objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_id(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Datum objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_date_created(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Email", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_shipping_email(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Cena celkem", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_total(true); ?></p>
                        </div>
                    </div>
                    <hr/>
                </div>

                <div class="col text-center">
                    <a class="btn btn-outline-secondary btn-sm mt-2"
                       href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>" target="_blank">
						<?php _e( "Zobrazit fakturu", "eso" ) ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>