<div class="container">
    <div class="row">
        <div class="col text-center">
			<?php _e( "Vaše objednávka byla přijata ke zpracování. Další informcae o Vaší objednávce najdete na své emailové adrese nebo ve Vašem profilu po prihlášení.", "eso" ) ?>
            <div id="order_detail" class="row">
                <div class="col-md-12">
                    <hr/>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Číslo objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_id(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Datum objednávky", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_date_created(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Email", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_shipping_email(); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <p><strong><?php _e("Cena celkem", "eso") ?>:</strong></p>
                            <p><?php echo $order->get_total(true); ?></p>
                        </div>
                    </div>
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>