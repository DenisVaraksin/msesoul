<?php
$checkout        = new Eso_Checkout();
$checkout_result = $checkout->checkout_result();

if ( isset( $_GET["order_id"] ) ) {
	$order = new Eso_Order( $_GET["order_id"] );
} else if ( isset( $_GET["refId"] ) ) { //Comgate
	$order = new Eso_Order( eso_get_order_id_by_comgate_payment_id( $_GET["refId"] ) );
} else if ( $_GET["id"] ) { //GoPay
	$order = new Eso_Order( eso_get_order_id_by_gopay_id( $_GET["id"] ) );
} else if ( $_GET["ORDERNUMBER"] ) { //GP WebPay
	$order = new Eso_Order( (int) $_GET["ORDERNUMBER"] );
} else {
	write_log( "Order not found on thankyou.php template" );
}

get_template_part( "pages/header" );

switch ( $checkout_result ) :
	case "cancelled" :
		include get_stylesheet_directory() . "/pages/thankyou-cancelled.php";
		break;
	case "waiting":
		include get_stylesheet_directory() . "/pages/thankyou-waiting.php";
		break;
	case "failed":
		include get_stylesheet_directory() . "/pages/thankyou-failed.php";
		break;
	default:
		include get_stylesheet_directory() . "/pages/thankyou-paid.php";
		break;
endswitch;
?>