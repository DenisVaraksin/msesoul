<form class="search" method="get" action="<?php echo home_url( 'siteurl' ); ?>" role="search" id="search-query">
	<input class="search-input form-control mr-sm-2" type="search" name="s" aria-label="Search" placeholder="<?php _e( 'Vyhledávání', 'theme' ); ?>">
	<button class="search-submit btn btn-text my-2 my-sm-0 text-white" type="submit" role="button"><?php _e( 'Hledat', 'theme' ); ?></button>
</form>


