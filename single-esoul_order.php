<?php get_header(); ?>
<section id="order_detail">
    <div class="container">
		<?php if ( have_posts() ): while ( have_posts() ) :
			the_post();
			$order = new Eso_Order( get_the_ID() );

			$author = get_post_field( "post_author", get_the_ID() );

			if ( $author != $order->get_customer_id() ) {
				die;
			}
			?>
            <div class="container">
                <div class="row">
                    <div class="col pt-5 mb-5 text-center">
                        <h1>
							<?php the_title(); ?>
                        </h1>
                    </div>
                </div>
                <div class="row mb-5">
					<?php
					if ( ! $order->is_paid() ) {
						$payment_method = $order->get_payment_method()->get_code();

						if ( $payment_method == "bank_transfer" ) {
							$fields = new Eso_Fields();
							$fields->render_bank_transfer_prompt( $order );
						} else if ( $payment_method == "gopay" || $payment_method == "comgate_payments" ) { ?>
                            <button id="single-esoul_order-pay" data-order="<?php echo $order->get_id() ?>"
                                    data-method="<?php echo $payment_method ?>" type="button"
                                    class="btn btn-primary"><?php _e( "Zaplatit objednávku", "eso" ) ?></button>
						<?php }
					}
					?>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <p><strong><?php _e( "Číslo objednávky", "eso" ) ?>:</strong></p>
                                <p>#<?php echo $order->get_id(); ?></p>
                            </div>
                            <div class="col-md-3">
                                <p><strong><?php _e( "Datum objednávky", "eso" ) ?>:</strong></p>
                                <p><?php echo $order->get_date_created(); ?></p>
                            </div>
                            <div class="col-md-3">
                                <p><strong><?php _e( "Email", "eso" ) ?>:</strong></p>
                                <p><?php echo $order->get_shipping_email(); ?></p>
                            </div>
                            <div class="col-md-3">
                                <p><strong><?php _e( "Cena celkem", "eso" ) ?>:</strong></p>
                                <p><?php echo $order->get_total( true ); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12 mt-5">
                        <table class="table table-xl-responsive">
                            <tr>
                                <td></td>
                                <td><?php _e( "Název", "eso" ) ?></td>
                                <td><?php _e( "Množství", "eso" ) ?></td>
                                <td><?php _e( "Cena za ks", "eso" ) ?></td>
                                <td><?php _e( "Celkem", "eso" ) ?></td>
                            </tr>
							<?php
							/* @var $order_item Eso_Order_Item */
							foreach ( $order->get_items() as $product_id => $product_data ) :
								$order_item = new Eso_Order_Item( $order->get_id(), $product_id );
								if ( ! isset( $order_currency ) ) {
									$order_currency = new Eso_Currency( $order_item->get_currency() );
								}
								?>
                                <tr>
                                    <td><?php $order_item->get_product()->the_featured_image( "table" ) ?></td>
                                    <td><?php echo $order_item->get_name() ?></td>
                                    <td><?php echo $order_item->get_quantity() ?><?php _e( "ks", "eso" ) ?></td>
                                    <td><?php echo $order_item->get_price_per_piece( $order_currency ) . " " . $order_currency->get_symbol(); ?></td>
                                    <td><?php echo $order_item->get_sum() . " " . $order_currency->get_symbol() ?></td>
                                </tr>
							<?php endforeach; ?>
                            <tr>
                                <td></td>
                                <td colspan="3"><?php echo $order->get_payment_method_name() ?></td>
                                <td><?php echo $order->get_payment_method_price( true ) ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3"><?php echo $order->get_shipping_method_name() ?></td>
                                <td><?php echo $order->get_shipping_method_price( true ) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-4">
                        <h3><?php _e( "Fakturační údaje", "eso" ) ?></h3>
                        <address>
							<?php
							echo $order->get_billing_company_name() . "<br/>";
							echo $order->get_billing_ico() . "<br/>";
							echo $order->get_billing_dic() . "<br/>";
							echo $order->get_billing_street() . "<br/>";
							echo $order->get_billing_postcode() . "<br/>";
							echo $order->get_billing_city() . "<br/>";
							$country = new Eso_Shipping_Zone( $order->get_billing_country() );
							echo $country->get_name() . "<br/>";
							?>
                        </address>
                    </div>
                    <div class="col-md-4">
                        <h3><?php _e( "Doručovací údaje", "eso" ) ?></h3>
                        <address>
							<?php
							echo $order->get_shipping_name() . "<br/>";
							echo $order->get_shipping_street() . "<br/>";
							echo $order->get_shipping_postcode() . "<br/>";
							echo $order->get_shipping_city() . "<br/>";
							$country = new Eso_Shipping_Zone( $order->get_shipping_country() );
							echo $country->get_name() . "<br/>";
							?>
                        </address>
                    </div>
                    <div class="col-md-4">
                        <h3><?php _e( "Kontaktní údaje" ) ?></h3>
						<?php
						echo $order->get_shipping_email() . "<br/>";
						echo $order->get_shipping_phone() . "<br/>";
						?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
						<?php
						if ( ! empty( $order->get_note() ) ) {
							?>
                            <h3><?php _e( "Poznámka", "eso" ) ?></h3>
							<?php echo $order->get_note(); ?>
							<?php
						}
						?>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-secondary"
                           href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>" target="_blank"><?php _e( "Zobrazit fakturu", "eso" ) ?></a>

                        <a class="btn btn-dark"
                           href="<?php eso_the_page_link( "orders" ) ?>"><?php _e( "Zpět na Mé objednávky", "eso" ) ?></a>
                    </div>

                </div>


            </div>
		<?php endwhile; ?>
		<?php else: ?>
            <h1><?php _e( 'Tato objednávka již neexistuje.', 'eso' ); ?></h1>
		<?php endif; ?>
</section>


<?php get_footer(); ?>
