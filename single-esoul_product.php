<?php get_header(); ?>
    <main role="main" class="pt-5">
        <div class="top_image additional-margin" style="background: url(<?php echo get_template_directory_uri() . "/img/kontakt.jpg"?>)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Detail produktu</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-lg-2 col-md-4">
                    <a href="<?php echo home_url("/produkty/")?>" class="black_button_big additional-width">zpět na produkty</a>
                </div>
                <div class="col-lg-10 col-md-8"></div>
            </div>
        </div>

        <?php ?>
        <?php if ( have_posts() ): while ( have_posts() ) :
        the_post(); ?>
        <?php
        $product = new Eso_Product( get_the_ID() );
        $currency = new Eso_Currency( get_the_ID() );
        ?>
        <div class="container" id="single-product-container">
            <div class="row">
                <!-- Left Side  of the product (images) -->
                <div class="col-sm-5">
                    <div class="product-gallery">


                        <div class="product-gallery__main">
                            <div class="produc_image-hover d-none">
                                <p class="bigger">+</p>
                            </div>

                            <div class="single-product-tags">
                                <?php $product->render_tags("product-detail"); ?>
                            </div>
                            <?php
                            if ( $product->get_featured_image_id() ) : ?>
                                <a href="<?php echo wp_get_attachment_image_url( $product->get_featured_image_id(), "photo" ) ?>" id="big-image-link"
                                   data-lightbox="<?php echo $product->get_id() ?>">
                                    <?php $product->the_featured_image( "large" ); ?>

                                </a>
                            <?php else:
                                echo eso_empty_thumbnail();
                            endif; ?>
                        </div>
                        <?php
                        $images = $product->get_images( false );
                        if ( ! empty( $images ) ) {
                            foreach ( $images as $key => $image_id ) { ?>
                                <div class="product-gallery__item">
                                    <a href="<?php echo wp_get_attachment_image_url( $image_id, "photo" ) ?>"
                                       data-lightbox="<?php echo $product->get_id() ?>">
                                        <?php echo wp_get_attachment_image( $image_id, 'little' ); ?>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>


                <!-- Right Side of the product  -->
                <div class="col-sm-7">
                    <div class="product-detail__info">
                        <h1>
                            <?php
                            the_title();
                            ?>
                        </h1>

                        <span id="more-text" class="show-read-more">
                            <?php the_content(); ?>
                        </span>
                        <?php if ( $product->is_purchasable() ) : ?>
                        <div class="product-detail__cta">
                            <div class="product-stav">
                                <div class="left-side mr-5">
                                    STAV:
                                </div>
                                <div class="right-side pl-2">
                                    <?php echo $product->get_stock_status_name();?>
                                </div>
                            </div>

                            <div class="product-variants-cont">
                                <?php $variants = $product->get_variants(); ?>
                                <?php
                                /* @var $variant Eso_Product_Variant */
                                foreach ($variants as $variant) : ?>
                                    <div class="variants-container">
                                        <label for="variants"><?php echo $variant->get_name(); ?></label>
                                        <div class="var-select-cont">
                                            <style>
                                                .var-select-cont:after {
                                                    content: '<?php echo get_template_directory_uri(); ?>/img/arrowdown.png';
                                                }var-select-cont
                                            </style>
                                            <select name="<?php echo $variant->get_slug() ?>">
                                                <?php foreach($variant->get_attributes() as $attribute) :  ?>
                                                    <option value="<?php echo $attribute["name"] ?>"><?php echo $attribute["name"] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>

                            <form action="" id="single-add-to-cart" class="add-to-cart-form "
                                  data-id="<?php the_ID() ?>">
                                <?php wp_nonce_field( ESO_NONCE ); ?>
                                <div class="form-row mb-2">
                                    <div class="col-auto">
                                        <div class="product-amount">
                                            množství:
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <label class="product-details__label">
                                            <input type="number"
                                                   class="form-control form-control-lg eso-product-quantity"
                                                   name="eso-product-quantity" min="1" required value="1"
                                                   data-product-name="<?php echo $product->get_name() ?>"
                                                   max="<?php echo $product->get_stock_amount() ?>"/>
                                        </label>
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn-custom-white add-to-cart">
                                            <img class="cart-white" src="<?php echo get_template_directory_uri(); ?>/img/shopping-cartwhite.png" alt="">
                                            <?php _e( "DO košíku", "eso" ) ?>
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <?php else : ?>
                                <div class="stock-status stock-status--<?php echo $product->get_stock_status_slug() ?>">
                                    <span class="stock-status__name"><?php echo $product->get_stock_status_name() ?></span>
                                </div>
                            <?php endif; ?>
                        </div>

                        <p class="product-detail__header">
                            <?php if ( $product->is_discounted( eso_get_active_currency() ) ) : ?>
                                <span class="price-before-discount"><?php echo $product->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
                            <?php endif; ?>
                            <span class="price__item">
                                <?php echo $product->get_price( eso_get_active_currency(), true, true ); ?>
                                <span class="dph-price"><?php echo round($product->get_price_before_discount_without_tax( eso_get_active_currency(), true ));
                                    ?>&nbsp;<?php echo eso_get_active_currency_symbol();
                                    _e(" bez DPH"); ?>
                                </span>
                            </span>
                        <div class="product-feed">
                            <div class="fb-feed">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=&t=" title="Share on Facebook" target="_blank" class="feed-link" onclick="location.reload(); window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/feedfb.png">
                                </a>
                            </div>

                            <div class="email-feed">
                                <a href="mailto:marketsoul.mcc@gmail.com" class="feed-link">
                                    Zeptat se
                                </a>
                            </div>
                        </div>
                        </p>

                        <?php the_tags( __( 'Tags: ', 'theme' ), ', ', '<br>' ); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- displaying related products -->
        <?php if ($product->get_related_products()) { ?>
        <section id="related-products">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="related-products-header">
                            <h2>Nejprodávanější produkty</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id="related-products-call">
                <div class="row first-row">
                    <?php
                    foreach ($product->get_related_products() as $related_product) {
                        ?>
                        <?php echo $related_product->get_list_item_template(); ?>
                        <?php
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="button-wrapper">
                            <a href="<?php echo home_url("/produkty/")?>" class="btn-custom-black">všechny produkty</a>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    <?php } else {?>
        <section id="related-products">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="button-wrapper">
                        <a href="<?php echo home_url("/produkty/")?>" class="btn-custom-black">všechny produkty</a>
                    </div>
                </div>
            </div>
        </div>
        </section>

    <?php }?>


        <!-- Contact Us Section -->
        <section id="singleproduct_banner" style="background: url(<?php echo get_template_directory_uri() . "/img/singleproductbanner.png"?>)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Potřebujete poradit? Neváhejte nás kontaktovat.</h2>
                        <div class="center_button">
                            <a href="<?php echo home_url("/kontakt/")?>" class="black_button all_product">kontakt</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

<?php get_footer(); ?>