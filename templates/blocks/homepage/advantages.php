<?php
/**
 * Block Name: Front Advantages
 *
 * @author Denis Varaksin
 * @since 02.10.2020
 */
?>

<section id="list_ico">
    <div class="container">
        <div class="row">
            <?php if (have_rows('msesoul-advantages')) :
            while (have_rows('msesoul-advantages')) : the_row();

                $picture = get_sub_field('advantages-image');
                $header = get_sub_field('advantages-header');
                $text = get_sub_field('advantages-text');
                ?>

            <div class="col-md-3">
                <img class="ico_single" src="<?php echo $picture ?>">
                <p class="title_par"><?php echo $header ?></p>
                <p><?php echo $text ?></p>
            </div>

            <?php
            endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
