<?php
/**
 * Block Name: Front Contact
 *
 * @author Denis Varaksin
 * @since 02.10.2020
 */
?>

<?php $bannerpicture = get_field('banner-picture');
      $bannertext = get_field('banner-text');
      $bannerbutton = get_field('banner-button');
?>

<section id="info_banner" style="background: url(<?php echo $bannerpicture?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $bannertext ?></h2>
                <div class="center_button">
                    <a href="<?php echo home_url("/kontakt/")?>" class="black_button all_product"><?php echo $bannerbutton ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
