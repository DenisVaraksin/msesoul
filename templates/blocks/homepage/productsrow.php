<?php
/**
 * Block Name: MSEsoul Popular Products (Second Products Row)
 *
 * @author Denis Varaksin
 * @since 02.10.2020
 */
?>

<section id="popular_product" class="product_list_home">
    <div class="container">
        <?php $title = get_field('title'); ?>

        <h2><?php echo $title; ?></h2>
        <div class="row">
            <!-- Get a specific product by ID for Homepage display Frusack Duo -->
            <?php
            $id2 = get_field('second-products-id');
            $id3 = get_field('kategorie');

            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_category',
                        'field' => 'id',
                        'terms' => $id3
                    )
                )
            );

            $query = new WP_Query($args);
            if ($query->have_posts()) {
                $counter = 1; ?>

                <?php while ($query->have_posts()) {
                    $query->the_post();
                    $product = new Eso_Product(get_the_ID());
                    ?>
                    <a href="<?php echo the_permalink($product->get_id()); ?>" id="product-item-<?php echo get_the_ID(); ?>"
                       class="col-md-3 single_product <?php if ($counter > 4) {
                           echo ' d-none';
                       }?>">
                        <div class="over_product">
                            <div class="product-tags">
                                <?php
                                $product->render_tags("product-detail");
                                ?>
                                    <!--
                            <div class="over_tag">
                                <div class="product-item__tag product-tag tag--nove">
                                    <?php// echo $product->get_tag_name(); ?>
                                </div>
                            </div>
                            -->
                            </div>

                            <div class="product_image">
                                <?php if ($product->get_featured_image_id()) {
                                    $product->the_featured_image();
                                } else {
                                    echo eso_empty_thumbnail();
                                } ?>
                                <div class="produc_image-hover d-none">
                                    <p class="bigger">+</p>
                                </div>
                            </div>

                            <h3 class="product-item__title"><?php echo $product->get_name(); ?></h3>
                            <div class="status">
                                <span class="font-weight-bold stock-status--in-stock"><?php echo $product->get_stock_status_name();  ?></span>
                            </div>

                            <div class="price price--list">
                            <?php if ( $product->is_discounted( eso_get_active_currency() ) ) : ?>
                                <span class="price-before-discount"><?php echo $product->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
                             <?php endif; ?>

                                <span class="price__item">
                                <?php echo $product->get_price(eso_get_active_currency(), true, true); ?>
                                </span>
                            </div>

                            <div class="product-item__purchase">
                                <div class="product-purchase">
                                    <form action="" class="add-to-cart-form"
                                          data-id="<?php echo $product->get_id() ?>">
                                        <?php wp_nonce_field(ESO_NONCE); ?>
                                        <div class="col-auto d-none">
                                            <label class="product-details__label">
                                                <input type="number"
                                                       class="form-control form-control-lg eso-product-quantity"
                                                       name="eso-product-quantity" min="1" required value="1"
                                                       data-product-name="<?php echo $product->get_name() ?>"
                                                       max="<?php echo $product->get_stock_amount() ?>"/>
                                            </label>
                                        </div>
                                        <div class="addtocart-button-wrapper">
                                            <button type="submit" class="btn-custom-white add-to-cart">
                                                <img
                                                        src="<?php echo get_template_directory_uri(); ?>/img/shopping-cart.svg"
                                                        alt="icon-cart">
                                                <?php _e("Do košíku", "eso") ?>
                                            </button>
                                            <div class="black_button_out">Detail</div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </a>
                    <?php
                    $counter++;
                }
            }
            ?>
        </div>

        <div class="center_button">
            <a href="<?php echo home_url("/produkty/")?>" class="black_button all_product">Všechny produkty</a>
        </div>
    </div>
</section>
