<?php
/**
 * Block Name: Front Slider
 *
 * @author Denis Varaksin
 * @since 02.10.2020
 */
?>

	<section id="slider">
        <?php if (have_rows('front-slider')) :
        while (have_rows('front-slider')) : the_row();
        $slider_picture = get_sub_field('slider-image');
        $slider_header = get_sub_field('slider-header');
        $slider_text = get_sub_field('slider-text');
        $slider_button_link = get_sub_field('front-slider-button-link');
        ?>
		<div class="single_slide" style="background: url(<?php echo $slider_picture?>)">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="over_slide">
							<h1><?php echo $slider_header ?></h1>
							<p><?php echo $slider_text ?></p>
							<a href="<?php echo get_permalink( get_page_by_title( 'Kontakt' ) )?>" class="black_button"><?php echo $slider_button_link ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
        endwhile;
        endif;
		?>
	</section>