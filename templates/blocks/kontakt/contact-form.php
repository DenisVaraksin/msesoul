<?php
/**
 * Block Name: Contact Form
 *
 * @author Denis Varaksin
 * @since 02.11.2020
 */
?>
<?php $contactheader = get_field('contact-form-header');
?>


<section id="contact_form">
    <div class="container">
        <h2><?php echo $contactheader; ?></h2>
        <div class="row">
            <div class="col-md-12">
                <?php echo do_shortcode('[contact-form-7 id="48" title="Contact form 1"]') ;?>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="divider"></div>
        </div>
    </div>
</div>



