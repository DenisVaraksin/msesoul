<?php
/**
 * Block Name: Contact Map
 *
 * @author Denis Varaksin
 * @since 02.11.2020
 */
?>

<?php $contacmapheader = get_field('contact-map-header');
?>


<section id="mapa">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $contacmapheader ?></h2>
            </div>
        </div>
    </div>

   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2605.921601769292!2d17.663660915863098!3d49.22101388305877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47137357d9073a55%3A0xf233d02b9a280e8!2sMarketSoul%20s.r.o.!5e0!3m2!1scs!2scz!4v1576780918723!5m2!1scs!2scz" width="100%" height="400" frameborder="0" style="border:0; margin-bottom: -7px;" allowfullscreen=""></iframe>

   <!--
    <div id="map-container">

    </div>-->

    <script>
        // Initialize and add the map
        function initMap() {
            // The location of Uluru
           // var uluru = {lat: 49.221011, lng: 17.665951};
            // The map, centered at Uluru
            //var map = new google.maps.Map(
                //document.getElementById('map-container'), {zoom: 12, center: uluru});
            // The marker, positioned at Uluru
           // var marker = new google.maps.Marker({position: uluru, map: map});

            var map = new google.maps.Map(document.getElementById('map-сщтефштук'), {
                center: {lat: 49.221011, lng: 17.665951},
                zoom: 12,
                styles: [
                    {
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });
            var uluru = {lat: 49.221011, lng: 17.665951};
            var marker = new google.maps.Marker({position: uluru, map: map});
        }


        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCl-AulDz7_KCpZQuiNDo9YUUT9GS2RojA
&callback=initMap"
            async defer></script>


</section>

</section>