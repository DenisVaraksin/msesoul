<?php
/**
 * Block Name: Contact Information
 *
 * @author Denis Varaksin
 * @since 02.11.2020
 */
?>

<?php $contactpicture = get_field('contact-image');
$contacttitle = get_field('contact-title');
$contacticon1 = get_field('contact-icon1');
$contacticon2 = get_field('contact-icon2');
$contacticon3 = get_field('contact-icon3');

$contactheader1 = get_field('contact-header1');
$contactheader2 = get_field('contact-header2');
$contactheader3= get_field('contact-header3');

$contacttext11 = get_field('contact-text1-1');
$contacttext12 = get_field('contact-text1-2');
$contacttext2= get_field('contact-text2');
$contacttext3= get_field('contact-text3');
?>

<div class="top_image" style="background: url(<?php echo $contactpicture; ?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo $contacttitle;?></h1>
            </div>
        </div>
    </div>
</div>

<section id="kontakt_info">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-3">
                        <img class="ico_kontakt" src="<?php echo $contacticon1; ?>">
                    </div>
                    <div class="col-md-9">
                        <h2><?php echo $contactheader1 ?></h2>
                        <div class="row">
                            <div class="col-md-6">
                                <?php echo $contacttext11; ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $contacttext12; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-3">
                        <img class="ico_kontakt" src="<?php echo $contacticon2;?>">
                    </div>
                    <div class="col-md-9">
                        <h2><?php echo $contactheader2 ?></h2>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $contacttext2; ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-3">
                        <img class="ico_kontakt" src="<?php echo $contacticon3; ?>">
                    </div>
                    <div class="col-md-9">
                        <h2><?php echo $contactheader3 ?></h2>
                        <div class="row">
                            <div class="col-md-12">
                                <p><a href="mailto:info@e-soul.cz"><?php echo $contacttext3; ?></a></p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="divider"></div>
            </div>
        </div>
    </div>
</section>

