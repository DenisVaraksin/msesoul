<?php
/**
 * Block Name: About
 *
 * @author Denis Varaksin
 * @since 02.11.2020
 */
?>

<?php $onaspicture = get_field('about-image');
$onasheader = get_field('about-header');
$textheader1 = get_field('about-textheader1');
$texparagraph1 = get_field('about-text1');
$textheader2 = get_field('about-textheader2');
$texparagraph2 = get_field('about-text2');
?>


<div class="top_image" style="background: url(<?php echo $onaspicture; ?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo $onasheader; ?></h1>
            </div>
        </div>
    </div>
</div>

<section id="content_text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $textheader1; ?></h2>
                <p><?php echo $texparagraph1; ?></p>

                <h2><?php echo $textheader2; ?></h2>
                <p><?php echo $texparagraph2; ?></p>
            </div>
        </div>
    </div>
</section>