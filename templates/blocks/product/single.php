<?php
/**
 * Block Name: Single Product Content
 *
 * @author Denis Varaksin
 * @since 02.12.2020
 */
?>
<div class="msesoul-single-product-content">
    <?php
    $single_text = get_field('msesoul-single-product-text');
    $single_text_more = get_field('msesoul-single-product-textmore');
    ?>
    <p>

    <?php// echo $single_text ?>


    <span id="more-text" class="show-read-more">
        <?php echo $single_text ?>
    </span>
    </p>
</div>