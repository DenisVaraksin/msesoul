<ul class="nav nav-pills justify-content-center" id="cart-nav">
	<li class="nav-item">
		<a class="nav-link active round-first" data-toggle="pill" href="#cart-content"><?php _e("Nákupní košík", "eso") ?></a>
	</li>
	<li class="nav-item <?php $cart = new Eso_Cart(eso_session_token()); if(count($cart->get_items()) < 1) echo "disabled"; ?>">
		<a class="nav-link" data-toggle="pill" href="#cart-customer"><?php _e("Dodací údaje", "eso") ?></a>
	</li>
	<li class="nav-item disabled">
		<a class="nav-link round-last" data-toggle="pill" href="#cart-summary"><?php _e("Souhrn", "eso") ?></a>
	</li>
</ul>