<?php
/**
 * @since 2019.7
 */
$cart = new Eso_Cart( eso_session_token() );
?>
<div id="cart-quick-look">
    <div class="container-fluid pt-3 pb-3">
        <div class="row">
            <div class="col">
                <strong><?php _e("V košíku máte", "eso") ?></strong>
            </div>
        </div>
    </div>
    <div class="container-fluid product-list">
		<?php
		/* @var $cart_item Eso_Cart_Item */
		foreach ( $cart->get_items() as $cart_item ) :
			$product = new Eso_Product( $cart_item->get_id() );
			?>
            <div class="row product-list__row">
                <div class="col-md-2 d-flex align-items-center cart-quick-look-pic">
					<?php if ( $product->get_featured_image_id() ) :
						echo wp_get_attachment_image( $product->get_featured_image_id(), "table" );
					else :
						echo eso_empty_thumbnail();
					endif; ?>
                </div>
                <div class="col-md-5 d-flex align-items-center">
					<?php echo $product->get_name(); ?>
                </div>
                <div class="col-md-2 d-flex align-items-center">
					<?php echo $cart_item->get_quantity() . " " . __( "ks", "eso" ) ?>
                </div>
                <div class="col-md-3 d-flex align-items-center">
					<strong><?php echo round($cart->get_sum( true, true )) . ' ';
                        echo eso_get_active_currency_symbol(); ?></strong>                                                   <!--  echo $cart_item->get_total_with_currency( true ) PHP before -->
                </div>
            </div>
		<?php endforeach ?>
    </div>
    <hr>
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-sm-5">
                <?php _e("Celkem s DPH", "eso") ?>
            </div>
            <div class="col-sm-3">
                <strong><?php echo $cart->get_sum(true, true) ?></strong>
            </div>
            <div class="col-sm-4">
                <a class="btn btn-secondary btn-sm" href="<?php eso_the_page_link("cart") ?>"><?php _e("Do košíku", "eso") ?></a>
            </div>
        </div>
    </div>
</div>
