<?php get_template_part("pages/header"); ?>
<section id="cart">
<div class="container">
    <div class="row">
        <div class="col pb-4">
            <?php get_template_part("templates/cart/cart", "nav"); ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form action="" id="eso-checkout" novalidate>
	            <?php wp_nonce_field( ESO_NONCE ); ?>
                <input type="hidden" name="action" value="eso_ajax">
                <input type="hidden" name="eso_action" value="process_order">
                <input type="hidden" name="checkout[options][currency]" value="<?php echo eso_get_active_currency_code() ?>">
                <input type="hidden" name="customer_id" value="<?php if(is_user_logged_in()) echo get_current_user_id(); ?>">
                <div class="tab-content">
                    <div id="cart-content" class="tab-pane fade show active">
		                <?php get_template_part("templates/cart/cart", "content"); ?>
                    </div>
                    <div id="cart-customer" class="tab-pane fade">
		                <?php get_template_part("templates/cart/cart", "customer"); ?>
                    </div>
                    <div id="cart-summary" class="tab-pane fade">
		                <?php get_template_part("templates/cart/cart", "summary"); ?>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
</section>