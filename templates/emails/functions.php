<?php
/**
 * @param $title
 *
 * @since 2019.6
 */
function eso_email_template_header( $title ) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo $title ?></title>
        <style type="text/css">
            body {
                background-color: #eeeeee;
                padding: 5px;
                margin: 0;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif
            }

            #wrapper {
                max-width: 700px;
                margin: 0 auto;
                padding: 15px;
            }

            #content {
                border: 1px solid #cbcbcb;
                padding: 15px;
                background: white;
            }

            #footer {
                margin-top: 1rem;
                font-size: .8rem;
            }

            .button {
                border: 2px solid black;
                border-radius: 100px;
                width: 180px;
                text-align: center;
                font-size: 14px;
                position: relative;
                background: white;
                display: block;
                margin: 1rem auto;
                color: black;
                text-decoration: none;
                font-weight: bold;
                padding: 15px 25px;
            }

            table {
                width: 100%;
            }

            td img {
                max-height: 50px;
            }

            img {
                width: auto;
            }

            tr.head {
                font-weight: bold;
            }

            h1 {
                text-align: center;
            }

            .button-area {
                margin-top: 2rem;
            }

        </style>
    </head>
    <body>
    <div id="wrapper">
    <div id="content">
<?php }

/**
 * @since 2019.6
 */
function eso_email_template_footer() { ?>
    </div>
    <div id="footer"><?php _e( "Email Vám odeslal obchod " ); ?>
        <a href="<?php echo home_url() ?>"><?php echo get_bloginfo( "name" ); ?></a>.
    </div>
</div>
</body>
</html>
<?php }