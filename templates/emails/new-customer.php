<?php
/**
 * This template is sent to the customer after registration
 *
 * @var $object_id
 */
$store    = new Eso_Store();
$customer = new Eso_Customer( $object_id );

eso_email_template_header( __($store->get_name() . " - Registrace", "eso" ) ); ?>

    <h1><?php _e( "Děkujeme Vám za registraci na našem e-shopu ". $store->get_name(), "eso" ) ?></h1>
	<br>
	<p>
		<?php echo __( "Vaše uživatelské jméno je:" . $customer->get_email(), "eso" ) ?>
	</p>
	<p>
		<?php _e("Heslo si vytvoříte po kliknutí na odkaz:", "eso") ?>
	</p>
	<a class="button"
	   href="<?php echo $customer->get_password_reset_url() ?>"><?php _e( "Nové heslo", "eso" ) ?>
	</a>
	<p>
		<?php _e("Přejeme hezký den", "eso") ?>
	</p>
	<p>
		<?php _e("Tým " . $store->get_name(), "eso") ?>
	</p>

<?php eso_email_template_footer(); ?>