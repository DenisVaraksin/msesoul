<?php
/**
 * This template is sent to store owner when a new order is made
 *
 * @var $object_id
 */
$order  = new Eso_Order( $object_id );
$store  = new Eso_Store();
$fields = new Eso_Fields();

eso_email_template_header( __( "Děkujeme za Vaši objednávku", "eso" ) ); ?>
    <h1><?php _e( "Děkujeme za Vaši objednávku na e-shopu ", "eso" ) ?><?php echo $store->get_name() ?></h1>
<?php if ( $order->get_payment_method()->get_code() == "bank_transfer" ) :
	$fields->render_bank_transfer_prompt( $order );
	echo "<div style='height: 50px;'></div>";
endif; ?>
    <table>
        <tr class="head">
            <td></td>
            <td><?php _e( "Název položky", "eso" ) ?></td>
            <td><?php _e( "Cena za ks", "eso" ) ?></td>
            <td><?php _e( "Množství", "eso" ) ?></td>
            <td><?php _e( "Celkem zboží s DPH", "eso" ) ?></td>
        </tr>
		<?php
		/* @var $item Eso_Order_Item */
		foreach ( $order->get_items() as $item ) : ?>
            <tr>
                <td><?php $item->get_product()->the_featured_image( "table" ) ?></td>
                <td><?php echo $item->get_name() ?></td>
                <td><?php echo $item->get_price_per_piece( $order->get_currency() ) . " " . $order->get_currency()->get_symbol() ?></td>
                <td><?php echo $item->get_quantity() ?></td>
                <td><?php echo $item->get_sum() . " " . $order->get_currency()->get_symbol() ?></td>
            </tr>
		<?php endforeach; ?>
        <tr class="footer">
            <td></td>
            <td></td>
            <td></td>
            <td><?php _e( "Doprava a platba", "eso" ) ?></td>
            <td><?php echo ($order->get_total() - $item->get_sum()) ?></td>
        </tr>
        <tr class="footer">
            <td></td>
            <td></td>
            <td></td>
            <td><?php _e( "Celkem bez DPH", "eso" ) ?></td>
            <td><?php echo $order->get_total_without_tax() ?></td>
        </tr>
        <tr class="footer">
            <td></td>
            <td></td>
            <td></td>
            <td><?php _e( "Celkem s DPH", "eso" ) ?></td>
            <td><?php echo $order->get_total() ?></td>
        </tr>
    </table>
    <div class="button-area">
        <a class="button"
           href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>"><?php _e( "Stáhnout fakturu", "eso" ) ?></a>
    </div>
<?php eso_email_template_footer(); ?>