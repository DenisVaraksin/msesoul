<?php
/*
Template Name: Kontakt
*/
get_header();?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<section id="kontakt" class="padding-section">
    <!-- content is being rendered in Gutenberg Main Page -->
    <?php the_content(); ?>

    <!-- "Contact Information Block" -->
    <!--
    <div class="top_image" style="background: url(<?php //echo get_template_directory_uri() . "/img/kontakt.jpg"?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php //echo the_title();?></h1>
			</div>
		</div>
	</div>
    </div>


<section id="kontakt_info">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3">
						<img class="ico_kontakt" src="<?php //echo get_template_directory_uri() . "/img/placeholder.svg"?>">
					</div>
					<div class="col-md-9">
						<h2>Firma s.r.o</h2>
						<div class="row">
							<div class="col-md-6">
								
								<p>Neznámá 1234/56<br/>
								Praha 000 00<br/>
								Česka republika</p>
							</div>
							<div class="col-md-6">
								<p><strong>IČ:</strong> 123456789</p>
								<p><strong>DIČ:</strong> CZ123456789</p>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3">
						<img class="ico_kontakt" src="<?php //echo get_template_directory_uri() . "/img/call.svg"?>">
					</div>
					<div class="col-md-9">
						<h2>Telefon</h2>
						<div class="row">
							<div class="col-md-12">
								<p><strong>Mobil:</strong> +420 123 456 789</p>
								<p><strong>Mobil 2:</strong> +420 123 456 789</p>
								<br/>
								<p><strong>Fax:</strong> +420 123 456 789</p>
							</div>
							
						</div>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3">
						<img class="ico_kontakt" src="<?php //echo get_template_directory_uri() . "/img/email.svg"?>">
					</div>
					<div class="col-md-9">
						<h2>E-mail</h2>
						<div class="row">
							<div class="col-md-12">
								<p><a href="mailto:info@e-soul.cz">info@e-soul.cz</a></p>
							</div>
							
						</div>

					</div>
				</div>
			</div>
			
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="divider"></div>
			</div>
		</div>
	</div>
</section>
-->

    <!-- "Contact Form" to the block  -->
    <!--
<section id="contact_form">

<div class="container">
	<h2>Kontaktní formulář</h2>
	<div class="row">
		<div class="col-md-12">



            <?php
			//require get_template_directory() . '/include/email_form.php';
			?>
			<?php
			//echo $zprava;
			//if($form_show){
			?>
			<form method="post" action="#contact">
				<div class="row">
					<div class="col-md-6">
						<div class="single-input">
							<label>Jméno: *</label>
							<input type="text" name="jmeno" value="<?php// echo (isset($_POST["jmeno"]) ? $_POST["jmeno"] : null);?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="single-input">
							<label>Příjmení: *</label>
							<input type="text" name="prijmeni" value="<?php// echo (isset($_POST["prijmeni"]) ? $_POST["prijmeni"] : null);?>">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-6">
						<div class="single-input">
							<label>E-mail: *</label>
							<input type="text" name="email" value="<?php// echo (isset($_POST["email"]) ? $_POST["email"] : null);?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="single-input">
							<label>Telefon:</label>
							<input type="text" name="telefon" value="<?php// echo (isset($_POST["telefon"]) ? $_POST["telefon"] : null);?>">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="single-area">
							<label>Zpráva: *</label>
							<textarea name="zprava"><?php// echo (isset($_POST["zprava"]) ? $_POST["zprava"] : null);?></textarea>
						</div>
					</div>
				</div>

				<div class="captcha">
				<div id='grc'></div>	
				</div>
				<br/>
				<div class="row">
					<div class="col-md-6">
					<p>* Povinné údaje</p>
					</div>
					<div class="col-md-6">
					<input class="send-form black_button" type="submit" name="email_form" value="Odeslat">
					</div>
				</div>
			</form>
			<?php
			//}
			?>

		</div>
	</div>
</div>
</section>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="divider"></div>
		</div>
	</div>
</div>
    -->

    <!-- "Contact Map" Block -->
       <!--
   <section id="mapa">
   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h2>Jak se k nám dostanete</h2>
           </div>
       </div>
   </div>

   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2605.921601769292!2d17.663660915863098!3d49.22101388305877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47137357d9073a55%3A0xf233d02b9a280e8!2sMarketSoul%20s.r.o.!5e0!3m2!1scs!2scz!4v1576780918723!5m2!1scs!2scz" width="100%" height="400" frameborder="0" style="border:0; margin-bottom: -7px;" allowfullscreen=""></iframe>
   </section>
   -->


</section>

<?php  endwhile; endif; ?>


<?php get_footer(); ?>