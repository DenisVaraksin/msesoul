<?php
/*
Template Name: O nás
*/
get_header();?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<section id="content" class="padding-section">

    <!-- content is being rendered in Gutenberg Main Page -->
    <?php the_content(); ?>

    <!-- layout to be moved to the blocks -->
    <!--
<div class="top_image" style="background: url(<?php// echo get_template_directory_uri() . "/img/kontakt.jpg"?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php// echo the_title();?></h1>
			</div>
		</div>
	</div>
</div>

<section id="content_text">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Lorem ipsum dolor sit amet</h2>
				<p>ut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. , consectetuer adipiscing elit. Pellentesque sapien. Etiam posuere lacus quis dolor. Aliquam erat volutpat. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Aenean fermentum risus id tortor. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<h3>Lorem ipsum dolor sit amet</h2>
				<p>ut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. , consectetuer adipiscing elit. Pellentesque sapien. Etiam posuere lacus quis dolor. Aliquam erat volutpat. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Aenean fermentum risus id tortor. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<?php //the_content();?>
			</div>
		</div>
	</div>
</section>
-->

</section>

<?php  endwhile; endif; ?>

<?php get_footer(); ?>