<?php
/**
 * @since 2020.2
 *
 * @var $this Eso_Product
 */
?>
<a href="<?php echo the_permalink($this->get_id()); ?>" class="col-md-4 single_product"
   id="product-item-<?php echo get_the_ID(); ?>">
    <div class="over_product">

        <div class="product-tags">
            <?php
            $this->render_tags("product-detail");
            ?>
        </div>


        <div class="product_image">
            <?php if ($this->get_featured_image_id()) {
                $this->the_featured_image();
            } else {
                echo eso_empty_thumbnail();
            } ?>
            <div class="produc_image-hover d-none">
                <p class="bigger">+</p>
            </div>
        </div>

        <h3 class="product-item__title"><?php echo $this->get_name(); ?></h3>
        <div class="status">
            <span class="font-weight-bold stock-status--in-stock"><?php echo $this->get_stock_status_name(); ?></span>
        </div>

        <div class="price price--list">
            <?php if ( $this->is_discounted( eso_get_active_currency() ) ) : ?>
                <span class="price-before-discount"><?php echo $this->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
            <?php endif; ?>

            <span class="price__item">
                <?php echo $this->get_price(eso_get_active_currency(), true, true); ?>
            </span>
        </div>

        <div class="product-item__purchase">
            <div class="product-purchase">
                <form action="" class="add-to-cart-form"
                      data-id="<?php echo $this->get_id() ?>">
                    <?php wp_nonce_field(ESO_NONCE); ?>
                    <div class="col-auto d-none">
                        <label class="product-details__label">
                            <input type="number"
                                   class="form-control form-control-lg eso-product-quantity"
                                   name="eso-product-quantity" min="1" required value="1"
                                   data-product-name="<?php echo $this->get_name() ?>"
                                   max="<?php echo $this->get_stock_amount() ?>"/>
                        </label>
                    </div>
                    <div class="addtocart-button-wrapper">
                        <button type="submit" class="btn-custom-white add-to-cart">
                            <img
                                    src="<?php echo get_template_directory_uri(); ?>/img/shopping-cart.svg"
                                    alt="icon-cart">
                            <?php _e("Do košíku", "eso") ?>
                        </button>
                        <div class="black_button_out">Detail</div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</a>





