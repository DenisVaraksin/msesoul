<?php
/*
Template Name: Seznam produktů
*/
get_header();?>
<!-- NOT USED DUE TO DIFFERENT STRUCTURE -->
<!-- Header Picture -->
<section id="seznam_produktu" class="padding-section">
    <div class="top_image" style="background: url(<?php echo get_template_directory_uri() . "/img/seznam_produktu.jpg"?>)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo the_title();?></h1>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="list_product_page">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
 				<section id="category_list">
                    <div class="single_cat">
                        <div class="main_cat">
                            <a href="<?php echo get_post_type_archive_link("esoul_product") ?>">Vše</a>
                        </div>
                    </div>
 					<div class="single_cat">
 						<div class="main_cat">
                           <?php foreach ( get_terms( ["taxonomy" => "product_category", "hide_empty" => false ] ) as $category ) {

                            if(isset($_GET["category_id"])) {
                            $catId = (int) $_GET["category_id"];
                            } ?>
                            <a href="?category_id=<?php echo $category->term_id ?>" class='<?php if($catId == $category->term_id) { ?>active-link <?php } ?> category-link'><?php echo $category->name ?> </a>&nbsp;
                            <?php } ?>
 						</div>
 					</div>
 				</section>


 				<section id="sidebar_banner" style="background: url(<?php echo get_template_directory_uri() . "/img/sidebar_banner.jpg"?>)">
 					<div class="container">
 						<div class="row">
 							<div class="col-md-12">
 								<p class="banner_title">Potřebujete poradit? <br/>Kontaktujte nás.</p>

 								<div class="contact_box">
 									<a href="tel: +420 123 456 789"><i class="fas box_ico fa-phone-square"></i>+420 123 456 789</a>
 									<br/>
 									<a href="mailto: info@e-soul.cz"><i class="fas box_ico fa-envelope-square"></i><span>info@e-soul.cz</span></a>
 								</div>
 							</div>
 						</div>
 					</div>
 				</section>

 				<section id="slider_box">
 					<?php
 					for ($i=0; $i < 5; $i++) { 
 					?>
 					<div class="single_box_slide">
						<div class="over_box_slide">
							<img class="ico_single" src="<?php echo get_template_directory_uri() . "/img/delivery-truck.svg"?>">
							<p class="box_title">Dodání do 24 hod.</p>

							<p>Nejvíce popularizováno byl Lorem Ipsum v šedesátých letech 20. století, kdy byly.</p>
						</div>
					</div>
 					<?php
 					}
 					?>
					

 				</section>
			</div>

			<div class="col-md-9">


			<section class="product_list_home">
				<div class="container">
					<div class="row">
						<?php
						for ($i=0; $i < 9; $i++) { 
						?>
						<a href="<?php echo home_url("/produkty/lampa-copper/")?>" class="col-md-4 single_product">
							<div class="over_product">
								<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Nové</div></div>
								<div class="over_tag"><div class="product-item__tag product-tag tag--nove">Doprava zdarma</div></div>

								<div class="product_image">
								<img width="250" height="159" src="<?php echo get_template_directory_uri() . "/img/lampa.jpg"?>" class="attachment-small size-small" alt="" srcset="<?php echo get_template_directory_uri() . "/img/lampa.jpg"?> 250w, <?php echo get_template_directory_uri() . "/img/lampa.jpg"?> 90w, <?php echo get_template_directory_uri() . "/img/lampa.jpg"?> 50w, <?php echo get_template_directory_uri() . "/img/lampa.jpg"?> 476w" sizes="(max-width: 250px) 100vw, 250px">

								</div>

								<h3 class="product-item__title">Lampa Copper</h3>
								<div class="status">
									<span class="font-weight-bold stock-status--in-stock">Skladem</span>
								</div>

								<div class="price price--list">
									<span class="price__item">2550 Kč</span>
						        </div>

								<div class="row action_detail">
									<div class="col-md-6 cart_in">
										<img src="<?php echo get_template_directory_uri() . "/img/shopping-cart.svg"?>">
										<p>Do košíku</p>
									</div>
									<div class="col-md-6">
										<div class="black_button_out">Detail</div>
									</div>
								</div>

							</div>
						</a>
						<?php
						}
						?>
					</div>
					<div class="center_button">
						<a href="<?php echo home_url("/seznam-produktu/")?>" class="black_button all_product">Všechny produkty</a>
					</div>
				</div>	
			</section>


			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>